Comprimir documents XML
=======================

MP4UF2A2T2

Tècniques eficients d’emmagatzematge de documents XML

Compressió de dades XML amb `gzip`
----------------------------------

-   Els documents XML poden ser grans, en tenir molta informació
    redundant, com ara els noms dels elements.
-   Per emmagatzemar i transmetre aquests document pot ser
    convenient comprimir-los.
-   L’algoritme de l’ordre `gzip` ofereix una gran eficiència comprimint
    fitxers de text.
-   L’ordre `gunzip` equival a l’ordre `gzip -d`.

Sintaxi de l’ordre `gzip`
-------------------------

-   Pot causar confusió el fet de que l’ordre pot treballar com a filtre
    o modificar fitxers *in place*.
-   Pot causar confusió el fet de que l’ordre `gunzip` accepta noms de
    fitxers en els que falti l’extensió `.gz`.
-   L’ordre `tar` pot fer servir internament `gzip` (opció `z`).
-   Sintaxi com a filtre: `tar cvf - x | gzip -c > x.tgz`.
-   Sintaxi *in place*: `gzip x; gunzip x.gz`.
-   L’ordre `bzip2` té sintaxi similar a `gzip` i ofereix superior
    nivell de compressió. La trobem en l’opció `j` de l’ordre `tar`, i
    amb ella es fa servir típicament l’extensió `.bz2`.

Enllaços recomanats
-------------------

-   Pàgina de manual gzip(1).
-   Pàgina de [l’autor de l’ordre](http://www.gzip.org/) `gzip`.
-   Pàgina del [projecte GNU sobre
    l’ordre](http://www.gnu.org/software/gzip/) `gzip`.

Pràctiques
----------

-   Verifica que l’ordre `xmllint` treballa de forma “transparent” amb
    fitxers comprimits (amb extensió `.gz`).
-   Fes un script del shell, de nom `ztar`, que cridant a les ordres
    `tar` i `gzip`, sigui equivalent a cridar a l’ordre
    `tar cvzf filename …`.
-   Fes un script del shell, de nom `zedit`, que cridant a les ordres
    `geany` i `gzip`, permeti editar fitxers comprimits (amb extensió
    `.gz`) de forma “transparent” (en una primera versió la nova ordre
    no cal que gestioni casos erronis).

