Conceptes bàsics dels DTDs
==========================

MP4UF1A2PE1

Prova escrita (PE1)

Característiques de la prova
----------------------------

La motivació per fer aquesta prova escrita és la de insistir en la
necessitat d’assimilar correctament tots els conceptes fonamentals
relacionats amb el llenguatge XML. No es tracta doncs d’una prova sobre
continguts avançats: tot el que en ella es demani s’ha de dominar
totalment.

### Tipus de prova

La prova tindrà el format clàssic: sense ordinadors, sense documentació
de suport, tan sols amb un full de paper on respondre les preguntes.

### Enunciat

El dia de realització de la prova el seu enunciat es lliurarà en paper,
i estarà disponible en format XHTML en el [repositori local](_PE1.html)
de fitxers.

### Criteri de qualificació

La prova aporta el 50% de la nota del resultat d’aprenentatge associat a
l’activitat.
