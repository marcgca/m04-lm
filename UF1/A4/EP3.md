Al directori *img* teniu les imatges que necessitareu per fer l'exercici.
 Fins a d'ara hem utilitzat o parlat de les propietats *color, background-color, 
 text-align, vertical-align* entre d'altres. En aquest exercici cal a més a  més que utilitzeu les següents propietats:
* with
* height
* border
* padding

Heu d'usar la vostra plantila xhtml. L'exercici pràctic s'implementarà en dos fitxers:
* EP3.html 
* EP3.css

Us ha de quedar algo semblant a això:

![EP3.html](screenshots/EP3.png)
